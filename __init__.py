import re
import json
import os
import platform
import numpy as np
import requests
import torch
import torch.nn as nn
import clip
from PIL import Image
import pythoncom
from PIL.PngImagePlugin import PngInfo
from win32com.propsys import propsys
from win32com.shell import shellcon
from pathlib import Path


class AestheticPredictor(nn.Module):
    def __init__(self, input_size):
        super().__init__()
        self.input_size = input_size
        self.layers = nn.Sequential(
            nn.Linear(self.input_size, 1024),
            nn.Dropout(0.2),
            nn.Linear(1024, 128),
            nn.Dropout(0.2),
            nn.Linear(128, 64),
            nn.Dropout(0.1),
            nn.Linear(64, 16),
            nn.Linear(16, 1)
        )

    def forward(self, x):
        return self.layers(x)


class ModelLoader:
    def __init__(self, model_path):
        self.model, self.device = self.load_model(model_path)

    def load_model(self, model_path):
        if not Path(model_path).exists():
            url = f"https://github.com/christophschuhmann/improved-aesthetic-predictor/blob/main/{model_path}?raw=true"
            r = requests.get(url)
            with open(model_path, "wb") as f:
                f.write(r.content)

        device = "cuda" if torch.cuda.is_available() else "cpu"
        pt_state = torch.load(model_path, map_location=torch.device(device=device))

        predictor = AestheticPredictor(768)
        predictor.load_state_dict(pt_state)
        predictor.to(device)
        predictor.eval()

        return predictor, device


class ImageProcessor:
    def __init__(self, device):
        self.device = device
        self.clip_model, self.clip_preprocess = clip.load("ViT-L/14", device=device)

    def get_image_features(self, image, model=None, preprocess=None):
        image = preprocess(image).unsqueeze(0).to(self.device)  # Änderung hier
        with torch.no_grad():
            image_features = self.clip_model.encode_image(image)  # Changed line
            image_features /= image_features.norm(dim=-1, keepdim=True)
        image_features = image_features.cpu().detach().numpy()
        return image_features

    def convert_image(self, image):
        i = 255. * image.cpu().numpy()
        img = Image.fromarray(np.clip(i, 0, 255).astype(np.uint8))
        return img


class FileTagger:
    def tag_file(self, filename, tag, overwrite=False):
        if platform.system() != "Windows":
            print("Nur auf Windows-Systemen unterstützt.")
            return

        pythoncom.CoInitialize()
        file_path = os.path.abspath(filename)
        pk = propsys.PSGetPropertyKeyFromName("System.Keywords")
        ps = propsys.SHGetPropertyStoreFromParsingName(file_path, None, shellcon.GPS_READWRITE,
                                                       propsys.IID_IPropertyStore)
        existing_values = ps.GetValue(pk).GetValue()
        if existing_values is None:
            existing_values = []

        if overwrite:
            new_values = [tag]
        else:
            if tag in existing_values:
                print(f'Tag "{tag}" ist bereits in der Datei {filename} vorhanden.')
                return
            new_values = existing_values + [tag]

        new_value = propsys.PROPVARIANTType(new_values, pythoncom.VT_VECTOR | pythoncom.VT_BSTR)
        ps.SetValue(pk, new_value)
        ps.Commit()
        print(f'Tag "{tag}" wurde zur Datei {filename} hinzugefügt.')


class FileSaver:
    def find_highest_counter(self, folder, prefix):
        files = os.listdir(folder)
        highest_counter = -1  # Startwert
        pattern = re.compile(f'{prefix}_(\d+)_\.png')  # Regex-Muster
        for file in files:
            match = pattern.match(file)
            if match:
                counter = int(match.group(1))
                if counter > highest_counter:
                    highest_counter = counter
        return highest_counter + 1  # gibt den nächsten verfügbaren Zähler zurück

    def save_file(self, file_path, img, metadata=None):
        png_info = PngInfo()
        if metadata is not None:
            for key, value in metadata.items():
                png_info.add_text(key, json.dumps(value))
        img.save(file_path, pnginfo=png_info, compress_level=4)
class TagAndSaveImage:
    def __init__(self):
        self.type = "processor"
        self.output_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'output'))
        self.prefix_append = ""

        model_loader = ModelLoader("sac+logos+ava1-l14-linearMSE.pth")
        self.predictor, self.device = model_loader.model, model_loader.device

        self.image_processor = ImageProcessor(self.device)
        self.file_tagger = FileTagger()
        self.file_saver = FileSaver()

    @classmethod
    def INPUT_TYPES(cls):
        return {
            "required": {
                "images": ("IMAGE",),
                "filename_prefix": ("STRING", {"default": "ComfyUI"})  # from SaveImage
            },
            "hidden": {"prompt": "PROMPT", "extra_pnginfo": "EXTRA_PNGINFO"}  # from SaveImage
        }

    RETURN_TYPES = ()  # from SaveImage
    FUNCTION = "tag_and_save_images"  # renamed method
    OUTPUT_NODE = True  # from SaveImage
    CATEGORY = "image"

    def get_score(self, image):
        image_features = self.image_processor.get_image_features(
            image, model=self.predictor, preprocess=self.image_processor.clip_preprocess
        )
        score = self.predictor(torch.from_numpy(image_features).to(self.device).float())
        return score.item()

    def tag_and_save_images(self, images, filename_prefix="ComfyUI", prompt=None, extra_pnginfo=None):
        filename_prefix += self.prefix_append
        full_output_folder = self.output_dir
        filename = filename_prefix
        counter = self.file_saver.find_highest_counter(full_output_folder, filename_prefix)
        subfolder = ""
        results = list()

        for image in images:
            img = self.image_processor.convert_image(image)

            # Score and tag the image
            score = self.get_score(img)
            tag = f'Score: {score:.1f}'

            # Prepare metadata
            metadata = {"Score": tag}
            if prompt is not None:
                metadata["prompt"] = json.dumps(prompt)
            if extra_pnginfo is not None:
                for key, value in extra_pnginfo.items():
                    metadata[key] = json.dumps(value)

            # Save the image
            file = f"{filename}_{counter:05}_.png"
            file_path = os.path.join(full_output_folder, file)
            self.file_saver.save_file(file_path, img, metadata)

            # Tag the file - Call tag_file method here
            self.file_tagger.tag_file(file_path, tag)

            results.append({
                "filename": file,
                "subfolder": subfolder,
                "type": self.type
            })
            counter += 1

        return results


NODE_CLASS_MAPPINGS = {
    "TagAndSaveImage": TagAndSaveImage
}

NODE_DISPLAY_NAME_MAPPINGS = {
    "TagAndSaveImage": "Tag and Save Image Node"
}
