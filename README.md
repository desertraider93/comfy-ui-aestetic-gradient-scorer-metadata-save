
# Aesthetic Image Scorer and Saver - Custom Node Extension for ComfyUI

This custom node extension for ComfyUI provides functionality to score images based on aesthetics, tag, and save them. The scoring is done using a pretrained machine learning model, and the images are tagged and saved with metadata including the aesthetic score.

## Acknowledgements

A special thanks to the [Improved Aesthetic Predictor](https://github.com/christophschuhmann/improved-aesthetic-predictor/) project which contributed to the development of the AestheticPredictor module used in this extension.

## Installation

1. Clone this repository into the `ComfyUI/custom_nodes` directory:
   ```bash
   cd ComfyUI/custom_nodes
   git clone git@gitlab.com:desertraider93/comfy-ui-aestetic-gradient-scorer-metadata-save.git
   ```

2. Navigate to the cloned repository directory:
   ```bash
   cd comfy-ui-aestetic-gradient-scorer-metadata-save
   ```

3. Install the required Python libraries listed in the `requirements.txt` file:
   ```bash
   pip install -r requirements.txt
   ```

4. Start ComfyUI normally.

## Usage

Once installed, this custom node extension adds a new node type, "Tag and Save Image Node", to ComfyUI. You can use this node to process a list of images, obtaining an aesthetic score for each image, tagging the images with their scores, and saving the images to disk.

## Code Structure

The code is organized into several classes:

- `AestheticPredictor`: Defines a PyTorch model for scoring images based on aesthetics.
- `ModelLoader`: Loads a pretrained model for scoring images.
- `ImageProcessor`: Processes images to extract features using the CLIP model, and converts tensor images to PIL images.
- `FileTagger`: Tags files with metadata.
- `FileSaver`: Saves images to disk, with optional additional metadata.
- `TagAndSaveImage`: The main class defining the custom node, which orchestrates the processing, scoring, tagging, and saving of images.

## Node Inputs

- `images`: A list of images to process.
- `filename_prefix`: (Optional) A prefix for the filenames of saved images. Defaults to "ComfyUI".
- `prompt`: (Hidden) An optional prompt to include in the metadata of saved images.
- `extra_pnginfo`: (Hidden) Optional additional metadata to include in the saved images.

## Node Outputs

This node returns a list of dictionaries, each containing:
- `filename`: The filename of a saved image.
- `subfolder`: An empty string (no subfolder is used).
- `type`: The string "processor".

## Configuration

You may need to adjust the following configuration options:
- The `model_path` argument to `ModelLoader` in the `TagAndSaveImage` constructor, to point to the location of the pretrained model file.
- The `output_dir` attribute of `TagAndSaveImage`, to specify the directory where images should be saved.

## Dependencies

This extension depends on several libraries, which can be installed using the provided `requirements.txt` file.

Make sure you have these libraries installed in your Python environment before using this extension.
